package com.crypto;

public class Task1 {
  //To solve the problem A:
  static void ProblemA(){
      float p1 = 1;
      float p2 = 0;
      //We have m = 2000 balls numbered from 1 to 2000 in an urn.
      for (int i = 0; i < 10000; i++){ //10000 just to be a hard bound (anything bigger than 2000 would work)
          p1 = p1 * (2000-i)/2000;
          p2 = 1-p1;
          if (p2 >= 0.5 ){ //for which n do we have a chance of at least 50% to draw the same ball at least twice?
             System.out.println("For n = " + (i+1) + " we have a chance of at least 50% to draw the some ball at least twice." );
             break;
          }
        }
  }
  //To solve the problem B:
  static void ProblemB(){
      float p1 = 1;
      float p2 = 0;
      //We have m = 2000 balls numbered from 1 to 2000 in an urn.
      //We pick one ball at n=1. At which n is the chance 50% to get a pair for the ball at n=1?
      for (int i = 0; i < 10000; i++){ //10000 just to be a hard bound (anything bigger than 2000 would work)
            p1 = p1 *  1999/2000;
            p2 = 1-p1;
          if (p2 >= 0.5 ){ //For L = 1/2 holds: n must be greater or equal than m/2, so the resulting n must be >= 1000
            //Result will be: n = 1385, which is bigger than 1000. So our condition holds.
            System.out.println("For n = " + (i+1) + " we have a chance of at least 50% to draw the first ball at least twice.");
            break;
          }

        }
  }

  public static void main(String[] args) {
        ProblemA();
        ProblemB();

  }
}
