package com.crypto;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.nio.charset.Charset;
import java.util.Random;

//Solution is modified from this base: https://automationrhapsody.com/md5-sha-1-sha-256-sha-512-speed-performance/

public class Task2 {

	public static void main(String[] args) {
		//System.out.println(generateStringToHash());
		double md5perseconds = (double)md5()/1000;
		double sha1perseconds = (double)sha1()/1000;
		double sha256perseconds = (double)sha256()/1000;
		//double sha512perseconds = (double)sha512()/1000;
		System.out.println("MD5: " + md5perseconds + " seconds per MiB ");
		System.out.println("SHA-1: " + sha1perseconds + " seconds per MiB ");
		System.out.println("SHA-256: " + sha256perseconds + " seconds per MiB ");

        System.out.println("MD5 is about 63% faster than SHA-1 for the first run. " +
                "(This might differ at later runs. There might be some kind buffering happening with our chosen library)");
        System.out.println("SHA-1 is about 74% faster than SHA-256 for the first run");
		//System.out.println("SHA-512: " + sha512perseconds + " seconds per MiB ");

		//Considering close to linear run time on hash functions: Modify input size by 1024*1024
		//MD5: O(N) + IV overhead
		System.out.println("MD5: " + (Math.round((1024*1024*md5perseconds)/60/60)) + " hours per TiB ");
		//SHA-1 runtime: O(N) + IV overhead
		System.out.println("SHA-1: " + (Math.round((1024*1024*sha1perseconds)/60/60)) + " hours per TiB ");
		//SHA-256 runtime: around O(N) + IV overhead
		System.out.println("SHA-256: " + (Math.round((1024*1024*sha256perseconds)/60/60))+ " hours per TiB ");
	}



	public static long md5() {
		StopWatch watch = new StopWatch();
		watch.start();
		DigestUtils.md5Hex(generateStringToHash());

		watch.stop();
		System.out.println(DigestUtils.md5Hex(generateStringToHash()));
		return watch.getTime();
	}

	public static long sha1() {
		StopWatch watch = new StopWatch();
		watch.start();
		DigestUtils.sha1Hex(generateStringToHash());

		watch.stop();
		System.out.println(DigestUtils.sha1Hex(generateStringToHash()));
		return watch.getTime();
	}

	public static long sha256() {
		StopWatch watch = new StopWatch();
		watch.start();
		DigestUtils.sha256Hex(generateStringToHash());
		watch.stop();
		System.out.println(DigestUtils.sha256Hex(generateStringToHash()));
		return watch.getTime();
	}

	public static long sha512() {
		StopWatch watch = new StopWatch();
		watch.start();

		DigestUtils.sha1Hex(generateStringToHash());
		watch.stop();
		System.out.println(DigestUtils.sha512Hex(generateStringToHash()));
		return watch.getTime();
	}

	public static String generateStringToHash() {
		// Medibit: 1024 * 1024 (1048576) bytes per second (8388608 bits per second)
		byte[] array = new byte[1048576]; // length is bounded by 1048576
		new Random().nextBytes(array);
		return new String(array, Charset.forName("ASCII"));
	}
}


