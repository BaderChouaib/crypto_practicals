package cryptography1_1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PermCryptor {
	static final int[] encryptKey = {1, 3, 0, 4, 2};

	public static void main(String[] args) throws Exception {

		System.out.println("Welcome to (De)Crypter!");
		System.out.println("=======================");
		System.out.println("Type in your plaintext: ");

        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
		String plaintext = inputReader.readLine();

		System.out.println("Typed in plaintext: " + plaintext);
		plaintext = paddPlaintext(plaintext);
		System.out.println("Padded Plaintext: " + plaintext);

		String ciphertext = encryptPlaintext(encryptKey, plaintext);
		String decryptedPlaintext = decryptCyphertext(ciphertext);

		System.out.println("Encrypted Ciphertext: " + ciphertext);
		System.out.println("Decrypted Plaintext: " + decryptedPlaintext);

		//Verify encryption function;
		if (encryptPlaintext(generateDecriptKey(encryptKey), encryptPlaintext(encryptKey,plaintext)).equals(plaintext)){
			System.out.println("Verification of encryption method successful");
		}else{
			System.out.println("Verification of encryption function failed!");
		}

	}

	static String encryptPlaintext(int[] encryptKey, String plaintext) {
		String ciphertext = "";
		//This variable ensures that we can go through cycles/parts of the plaintext 
		int loopInd = 0;
		for (int i=0; i<plaintext.length(); i++){
			//ensures that we only loop through indizes of the cryption key...
			int kInd = encryptKey[i%encryptKey.length];
			//but we still need to index through the plaintext. 
			if(i%encryptKey.length == 0 && i>0)
				loopInd++;
			var cipherConcat = Character.toString(plaintext.charAt(kInd+loopInd*encryptKey.length));
			ciphertext = ciphertext.concat(cipherConcat);
		}
		return ciphertext;
	}

	static String decryptCyphertext(String cyphertext) {
		int[] decryptKey = generateDecriptKey(encryptKey);
		return encryptPlaintext(decryptKey, cyphertext);
	}


	static int[] generateDecriptKey(int[] encryptKey) {
		int[] decryptKey = new int[encryptKey.length];

		for (int i=0; i<encryptKey.length; i++) {
			decryptKey[encryptKey[i]] = i;
		}
		return decryptKey;
	}

	static String paddPlaintext(String plaintext) {
		while(plaintext.length()%5 != 0) {
			plaintext = plaintext.concat("x");
		}
		return plaintext;
	}


}
