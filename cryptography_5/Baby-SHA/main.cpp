/*
baby_sha -- A weak cryptographic hash function for educational purpose.
Copyright (C) 2010 Christian Maaser

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1 of the
License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <iostream>
#include <string>

#include <string.h>
#include <chrono>

using namespace std;

string numerals = "0123456789";
string alphabetLower = "abcdefghijklmnopqrstuvwxyz";
string alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
string specialAnsi = "!\"#�%&/()=?`^�*';:_,.";

std::chrono::time_point<std::chrono::system_clock> starttime, endtime;

string relevantAsciiChars()
{
    string str;
    for (char i = 32; i < 127; ++i) {
        str += i;
    }
    return str;
}

// rotate bits left
unsigned char rol(unsigned char value, unsigned char bits)
{
    return value << bits | value >> (8 - bits);
}

union Block
{
    unsigned int data;
    char buffer[16];
};

union State
{
    unsigned int dword;
    char byte[4];
};

const State DEFAULT_IV = {0x8F1BBCDC};

State baby_sha(Block block, State iv)
{
    // expand data block from 4 bytes to 16 bytes.
    for (unsigned int i = 4; i != 16; ++i)
        block.buffer[i] = rol(block.buffer[i - 4] + block.buffer[i - 3] + block.buffer[i - 2] + block.buffer[i - 1], 5);

    // save state
    unsigned char a = iv.byte[0];
    unsigned char b = iv.byte[1];
    unsigned char c = iv.byte[2];
    unsigned char d = iv.byte[3];

    // for each data byte
    for (unsigned int i = 0; i != 16; ++i)
    {
        unsigned char e = b & c | d & (b | c);
        unsigned char t = rol(a, 3) + e + 0xCA + block.buffer[i];
        d = c;
        c = b;
        b = a;
        a = t;
    }

    // add new state to initial state
    iv.byte[0] += a;
    iv.byte[1] += b;
    iv.byte[2] += c;
    iv.byte[3] += d;

    return iv;
}

State baby_sha(const char* data, unsigned int dataLength, State iv = DEFAULT_IV)
{
    unsigned int offset = 0;
    while (dataLength != 0) {
        Block block;
        block.data = 0;
        for (unsigned int i = 0; i != 4 && dataLength > 0; ++i, --dataLength, ++offset)
            block.buffer[i] = data[offset];
        iv = baby_sha(block, iv);
    }
    return iv;
}

State test_baby_sha(std::string text, State iv = DEFAULT_IV)
{
    State result = baby_sha(text.c_str(), text.length(), iv);
    //std::cout << std::hex << "baby_sha(\"" << text << "\", " << iv.dword << ") -> " <<
      //           result.dword << std::dec << std::endl;
    return result;
}
// brute force function
void find_input_for_hash(string source, unsigned int hash, string input , size_t length)
{

	if (length == 0)
	{
		// if input hash matches with input hash
		auto testhash = test_baby_sha(input);
		if (testhash.dword == hash)
		{
			// calculate time used
            endtime = std::chrono::system_clock::now();
            int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>
                             (endtime-starttime).count();

			// output
			cout << std::hex << "Source: " << input << ", Hash: 0x" << testhash.dword
				 << endl << std::dec << "Time: " << elapsed_seconds << " seconds";
			cout << endl << endl;
		}
        return;
	}

	// iterate through the input stringspace
	for (size_t i = 0; i < source.length(); i++)
	{
		string appended = input + source[i];

		// continue the loop
		find_input_for_hash(source, hash, appended, length - 1);
	}
}

void algorithm_1a()
{
    // our targethash, which we are going to crack
	unsigned int targetHash = 0xd44a0fd4;

	// loop our find_input_for_hash function
    starttime = std::chrono::system_clock::now();
	while (1)
	{
		// string startlength
		static unsigned int strlen = 1;
		find_input_for_hash(alphabetLower + alphabetUpper + numerals + specialAnsi,
			targetHash, "", strlen);

		strlen++; // increment the seed length after each iteration
	}
}

void algorithm_1b()
{
    //Calculations:
    //128 ascii characters. We pick four of them in a random order (8 bit = 1 ascii character/32 bit = 4 ascii characters)
    //128 * 128 * 128 * 128 options = 268.435.456 options
    //We need expected amount of options = 268.435.456 / 2 = 134.217.728 options
    //SHA1 can hash in 590ms per 1M instructions + 10ms overhead for preparing string/cpu etc.
    //SHA1 = 600/1000000 = 600 nanoseconds per option.
    //Assuming linear execution time: BabySHA should be around five times fast (1/5 output bits, 1/5 rounds) = 120 nanoseconds per option
    //134.217.728 * 120ns = 16.106,12 ms / 16,106 seconds
    //Correction: Not 128 characters possible, but instead 96 (first 32 are not possible).

    //--Actual solution
    //2^31 tries. Comes from n = 32 for bits, and the formular 2^n-1 (average time to 2^n/2 = 2^n-1)

    // our targethash, which we are going to crack
	unsigned int targetHash = 0x4eb7689e;

	// loop our find_input_for_hash function
    starttime = std::chrono::system_clock::now();
	while (1)
	{
		// string startlength
		static unsigned int strlen = 4;
		find_input_for_hash(relevantAsciiChars(),
			targetHash, "", strlen);
	}
}

int main(int argc, char* argv[])
{
    test_baby_sha("");
    test_baby_sha("Hello world");
    test_baby_sha("Hellp world");
    test_baby_sha("Hellq world");
    test_baby_sha("Iello world");
    test_baby_sha("Jello world");
    test_baby_sha("Hello worle");
    test_baby_sha("Hello worlf");
    test_baby_sha("a");
    test_baby_sha("b");
    test_baby_sha("c");
    test_baby_sha("d");

    // Your code goes here ;o

    //solution for 1a.)
    test_baby_sha("w34K");

    //algorithm for 1a.):
    //algorithm_1a();

    //solution for 1b.)
    test_baby_sha("exe1");
    //algorithm for 1b.):
    //possible characters
    std::cout << "possible characters: " << relevantAsciiChars() << std::endl;
    algorithm_1b();

    //Task 2:
    // S is solution. Alice constructs h(S) = P, Bob receives P.
    // Bob can still brute force. So P is still a verifier
    // Other ways to prevent Bob to have verifier?
    // Alice could salt the input S with r. Then send S, r to Bob aswell
    // Bob could restrict Alice by sending her the original S. But Alice could cheat then by faking r (r not correct)
    // How could we prevent Alice from cheating? r needs to be big or r is the XOR of r1 from Bob and r2 from Alice.

    return 0;
}

